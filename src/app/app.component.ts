import { Component, OnInit } from '@angular/core';
import * as THREE from 'three';
import OrbitControls from 'three-orbitcontrols';
import * as TWEEN from '@tweenjs/tween.js';
import { Geometry } from 'three';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})

export class AppComponent implements OnInit {

  canvas: HTMLCanvasElement;
  renderer: THREE.WebGLRenderer;
  camera: THREE.PerspectiveCamera;
  scene: THREE.Scene;
  light: THREE.AmbientLight;
  pointLight: THREE.PointLight;
  cube: THREE.Mesh;
  tween: TWEEN.Tween;
  controls: any;

  width = window.innerWidth;
  height = window.innerHeight;

  cubeScale = {
    x: 1,
    y: 1,
    z: 1
  }

  cubeScaleTarget = {
    x: 1,
    y: 1,
    z: 2
  };


  ngOnInit() {

    this.canvas = <HTMLCanvasElement>document.getElementById('canvas');

    this.renderer = new THREE.WebGLRenderer({
      canvas: this.canvas,
      alpha: true,
      antialias: true
    });

    this.renderer.setSize(this.width, this.height);
    this.scene =  new THREE.Scene();

    this.camera = new THREE.PerspectiveCamera(
      75, this.width / this.height, 0.1, 1000
    );

    this.camera.position.z = 4;
    this.scene.add(this.camera);

    this.controls = new OrbitControls(this.camera, this.renderer.domElement);

    // light
    this.light = new THREE.AmbientLight( new THREE.Color('#fff') );
    this.light.position.z = 50;
    this.scene.add(this.light);

    this.pointLight = new THREE.PointLight( new THREE.Color('pink'), 1.25, 1000 );
    this.pointLight.position.set( 0, 0, 100 );
    this.scene.add(this.pointLight);

    let geometry = new THREE.BoxGeometry( 1, 1, 1, 10, 10  );
    let material = new THREE.MeshBasicMaterial( {
      color: new THREE.Color('red'),
      // wireframe: true
    });

    this.cube = new THREE.Mesh( geometry, material );
    this.scene.add(this.cube);

    this.tween = new TWEEN.Tween(this.cubeScale)
                .to(this.cubeScaleTarget, 2000)
                .delay(1000)
                .easing(TWEEN.Easing.Elastic.InOut)
                .repeat(Infinity)
                .yoyo(true)
                .start();

    this.animate();

  }

  render = (timestamp: number) => {

    requestAnimationFrame( (timestamp) => {
      this.render(timestamp);
    });

    // this.cube.rotation.z += 0.01;
    this.cube.rotation.y += 0.01;

    this.tween.update(timestamp);

    this.tween.onUpdate( () => {
      this.cube.scale.x = this.cubeScale.x;
      this.cube.scale.y = this.cubeScale.y;
      this.cube.scale.z = this.cubeScale.z;
    });

    this.renderer.render(this.scene, this.camera);
  }

  animate = () => {

    window.addEventListener('DOMContentLoaded', (dom) => {
      this.render(dom.timeStamp);
    });

    window.addEventListener('resize', () => {
      this.width = window.innerWidth;
      this.height = window.innerHeight;
      this.resize();
    });

  }

  resize = () => {
    this.camera.aspect = this.width / this.height;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(this.width,this.height);
  }

}